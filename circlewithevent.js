﻿var mycircle=function(canvas,x,y)
{
    this.canvas = canvas;
    this.context = this.canvas.getContext("2d");
    this.x = x;
    this.y = y;
    this.r = 25;
    this.x1=[]
    this.y1=[]
    //this.k=0;
    this.mycolor = 'red';
    
}

mycircle.prototype.draw = function ()
{
    context.beginPath();
    context.arc(this.x, this.y, this.r, 0, 2*Math.PI, true);
    context.fillStyle = this.mycolor
    context.fill();
    context.strokeStyle = "black";
    context.lineWidth = 3
    context.stroke();

}
mycircle.prototype.setconnection=function(x,y)
{
	this.x1.push(x);
	this.y1.push(y);
	this.connect=1;
}
mycircle.prototype.drawline=function()
{
    for (var k=0;k<this.x1.length;k++)
    {
  	this.context.beginPath();
        this.context.moveTo(this.x, this.y);
        this.context.lineTo(this.x1[k], this.y1[k]);
        this.context.strokeStyle = "black";
        this.context.lineWidth = 3;
        this.context.stroke();

    }
}
